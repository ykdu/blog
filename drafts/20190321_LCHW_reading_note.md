## Learn C the Hardway (draft)

[HOME](../README.md)

阅读版本: [《笨办法学C 中文版》 - 飞龙译](https://github.com/wizardforcel/lcthw-zh)

## 阅读前言

* It is hard to write secure and solid code with C language

* Undefined Behavior, know it, try your best to avoid it

* 作者一直再强调自己书上面方法的正确性，并且受害妄想症般觉得别人会diss他。
所以书中可能会介绍，如何写正确（安全）的代码，为什们要这样写以及如何证明别人写的代码不正确。

* `Defensive-Programming`, assume flaws and prevent them

* gcc commands
    - `-E`: preprocess only
    - `-S`: compile, 
    - `-c`: compile and assembe get object file
    - `objdump`: dump the informations of obj or exe


## Just Do It 

* 测试环境：
    * 编译器 gcc
    * Debian Virtual Machine - 2.0GB Memory

1. 在你的文本编辑器中打开ex1文件，随机修改或删除一部分，之后运行它看看发生了什么 `via ex1`

    |操作\位置|头部(H)|中部(M)|尾部(T)|
    |:-:|:-:|:-:|:-:|
    |增加(A)|命令行报错|UB|测试正常|
    |删除(D)|命令行报错|UB|-|
    |修改(D)|命令行报错|UB|UB|
    * 主要还是不能够破坏了文件的结构

2. 传入的参数比printf少的时候将会产生UB `via ex1, ex3`
    ```assembly
    # corresponding ASM via objdump -S -g
        .loc 1 12 5
        movl	$97, 4(%esp)
        movl	$LC6, (%esp)
        call	_printf
     */
    printf("return value of putchar: %c %f %d %p %s %d %s %s \n\0", 0x61);
	401497:	c7 44 24 04 61 00 00 	movl   $0x61,0x4(%esp)
	40149e:	00 
	40149f:	c7 04 24 b8 50 40 00 	movl   $0x4050b8,(%esp)
	4014a6:	e8 c5 26 00 00       	call   403b70 <_printf>

    # .rodata content via objdump -s
   	4050b0 61723a20 25660a00 72657475 726e2076  ar: %f..return v
	4050c0 616c7565 206f6620 70757463 6861723a  alue of putchar:
	4050d0 20256320 25662025 64202570 20257320   %c %f %d %p %s 
	4050e0 25642025 73202573 200a0000 4e585048  %d %s %s ...NXPH
    ```
    * **可能产生原因**: 
        - 栈传参数(mingw-gcc): `_printf`函数调用的时候进行了参数压栈，
        那么这样意味着`_printf`会根据fmt string中的需要格式化替换的参数，从栈上进行取值。
        例子中fmt中需要进行替换的地方有8个`%c %f %d %p %s %d %s %s`，
        但是`_printf`的调用之前利用esp寄存器对两个栈上的内容进行了初始化（fmt string一次，0x61参数一次），
        如果`_printf`根据fmt字符串从栈上取8个参数，那么会有7个地址是非法的。因此产生了UB。
        - 寄存器传参(gcc)：`printf`函数会根据fmt当中的格式化字符串的数量取多个寄存器的值，
        如果在调用之前不曾传递足够的字符串为所有的寄存器初始化，那么将会有一部分的寄存器的值处于不确定的状态。

3. make `via ex2`
    * 通常作为一个自动化的编译工具被人们认识，
    make能够自动的判断那些发生了变化的文件，
    并按照用户的定义的makefile规则（包括文件之间的关系以及相关目标的操作）进行处理。
    这些规则可以是编译命令当然也可以是其他命令。
    * 默认会执行第一个target，而不会有什么的`default target`。
    * `.PHONY`，关键字表示某一个目标是虚拟的目标，即使存在同名的文件也会按照makefile中的定义优先进行处理。
    * 支持使用include语句来读取其他的Makefile文件，
    如果发生同名的target的情况，最后处理的target将会覆盖前面所有并且打印警告。
    * 老生常谈的-j参数，使得make会并行的处理相关Makefile中指定的target。
    该参数制定了并行执行的make实例的上限。

4. gcc `via ex2`
    * gcc全称是GNU C Compiler，常见于Linux系统，可以理解成为一个platform-specific的概念。
    * 编译源文件主要执行四个阶段：preprocessing(-E)，compilation(-S)，assembly(-c)以及linking
    编译器可以通过相关的参数，生成相关阶段的产物。
    * 由于gcc可以接受的参数很多，其中不免包括很多multi-letter参数，
    因此多个single-letter的参数不支持组合在一起, e.g., `-dv`和`-d -v`不同。
    * 常用参数：
        - `-I{dir}` 将`{dir}`添加到头文件的搜索路径
        - `-L{dir}` 将`{dir}`添加到搜索路径
        - `-l {lib} or -l{lib}` 在编译的时候，将会查找并且链接`{lib}`库

5. [valgrind](https://en.wikipedia.org/wiki/Valgrind) `via ex4`
    * Requirements: automake, libc6-dbg
    * 用来进行[memory debug](https://en.wikipedia.org/wiki/Memory_debugger),
    memory leak detection以及[profiling](https://en.wikipedia.org/wiki/Profiling_(computer_programming))的编程工具
    * 实际上valgrind是一个使用JIT（[just-in-time](https://en.wikipedia.org/wiki/Just-in-time_compilation)）技术的virtual machine。目标程序不会直接运行在host processor上面，其工作流程如下：
        - 将目标程序翻译为IR（Intermediate Representation, 中间表示）
        - 基于Memcheck等工具对IR形式转换修改（自动插桩），之后valgrind将修改后的IR转换为机器码，然后在host processor上面执行
        - valgrind还集成了GDB stub，允许GDB对valgrind上执行的target program进行测试
    * 主要检查以下几种情况：
        - 使用了未初始化的内存
        - 在对内容执行了free之后对内存进行访问
        - 越界访问malloc分配的内存块后面的内存
        - 内存泄露
        - ......
    * Memcheck工具无法检测到与栈分配内存相关的错误
    ```c
	int func(void){
		int Stack[5];
		Static[5] = 0;  /* Error - Static[0] to Static[4] exist, Static[5] is out of bounds */
		Stack [5] = 0;  /* Error - Stack[0] to Stack[4] exist, Stack[5] is out of bounds */
		return 0;
	}
    ```
    * ex4练习运行结果

    ```
    I am -16777128 years x (null) �� old.
    ==19630== Conditional jump or move depends on uninitialised value(s)
    ==19630==    at 0x4E807AD: vfprintf (vfprintf.c:1636)
    ==19630==    by 0x4E88228: printf (printf.c:33)
    ==19630==    by 0x1086EC: main (ex4.c:9)
    ==19630==
    ==19630== Use of uninitialised value of size 8
    ==19630==    at 0x4E7D19B: _itoa_word (_itoa.c:179)
    ==19630==    by 0x4E81899: vfprintf (vfprintf.c:1636)
    ==19630==    by 0x4E88228: printf (printf.c:33)
    ==19630==    by 0x1086EC: main (ex4.c:9)
    ==19630==
    ==19630== Conditional jump or move depends on uninitialised value(s)
    ==19630==    at 0x4E7D1A5: _itoa_word (_itoa.c:179)
    ==19630==    by 0x4E81899: vfprintf (vfprintf.c:1636)
    ==19630==    by 0x4E88228: printf (printf.c:33)
    ==19630==    by 0x1086EC: main (ex4.c:9)
    ==19630==
    ==19630== Conditional jump or move depends on uninitialised value(s)
    ==19630==    at 0x4E819A1: vfprintf (vfprintf.c:1636)
    ==19630==    by 0x4E88228: printf (printf.c:33)
    ==19630==    by 0x1086EC: main (ex4.c:9)
    ==19630==
    ==19630== Conditional jump or move depends on uninitialised value(s)
    ==19630==    at 0x4E80861: vfprintf (vfprintf.c:1636)
    ==19630==    by 0x4E88228: printf (printf.c:33)
    ==19630==    by 0x1086EC: main (ex4.c:9)
    ==19630==
    ==19630== Conditional jump or move depends on uninitialised value(s)
    ==19630==    at 0x4E808E2: vfprintf (vfprintf.c:1636)
    ==19630==    by 0x4E88228: printf (printf.c:33)
    ==19630==    by 0x1086EC: main (ex4.c:9)
    ==19630==
    ```

6. 变量的类型问题 `via ex6`
    ```
    // segment fault caused
    char ch = 'A';
    printf("%s", ch);
    ```
`printf`中的`%s`会打印一串字符串, 也会要求传入一个字符串指针，
指针变量的值实际上就是内存的地址，可以当作一个无符号的整型来看待。
    - 栈传参，`printf`将会从栈帧中的ch的位置上取出大小为sizeof(char*)的数据, 然而调用之前只初始化了sizeof(char)大小的数据
    - 寄存器传参，`printf`将会把ch所对应的参数寄存器的值作为一个char*来处理，因此访问0x41地址出现segment fault

7. 为什么char与int可以进行算术运算 `via ex7`
    * char类型实际上就是一个整数，其数值可以对应ascii码表上面的对应字符
    * 通过汇编代码可以看到
        * 初始化`nul_byte`的时候只初始化了一个`byte`大小的数字
        * 进行乘法之前，将操作数传递给相关寄存器的时候进行了扩展`movsbl`，即将一个`byte`扩展成为了一个`long`
    ```assembly
    char nul_byte = '\0';
    781:   c6 45 d7 00             movb   $0x0,-0x29(%rbp)
    int care_percentage = bugs * nul_byte;
    785:   0f be 45 d7             movsbl -0x29(%rbp),%eax
    789:   0f af 45 fc             imul   -0x4(%rbp),%eax
    78d:   89 45 d0                mov    %eax,-0x30(%rbp)
    ```

8. inline与内联函数
    * 内联函数会在定义的函数体将会在调用的地方展开，省去函数调用的开销以及参数传递的开销。
    * inline关键词只是对编译器的内联建议，编译器会根据情况自行判断是否将被inline修饰的函数处理成为内联函数
    * 由于C语言是分别编译之后进行链接，[所以需要注意的是](https://stackoverflow.com/questions/216510/extern-inline/216546#216546)：
        * inline关键词可能会使得被修饰的函数成为真正的内联函数，但是于此同时也会有一个非内联函数定义通过头文件传递给了其他的源文件。
        因此在编译过程中其他的源文件可能会调用这个函数，然后实际的编译过程改函数没有出现在linkable object当中，
        导致了在链接过程中，其他的文件中调用了该函数的地方会出现undefined function报错。
        （如果那些其他的源文件也将该文件当作inline进行处理的话，那么连接的过程中会遇见duplicated symbols的报错）
        * `external inline` 与 `static inline`在一定程度上可以解决连接过程中所遇见的问题 **TODO**
