# Book Lists

## Contents

* [HOME](../README.md)
* [Base C Part](#Base-C-Part)

## Base C Part

- [ ] The C Programming Language
    * 这本书较多的基础内容，之前都是略读着跳过

- [ ] Learn C the hardwary ([Download](https://drive.google.com/file/d/1g35-0TJ0DLWq4RpHlChr0snmSJ0EzFqu/edit))
    * 探索什么`UB`（Undefined Behavior），以及如何防止`UB`
